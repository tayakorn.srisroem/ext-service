package com.toffy.trainer.extjs.emp.model;

import lombok.Data;

@Data
public class PositionModel {
    Integer id;
    String code;
    String description;
}
