package com.toffy.trainer.extjs.emp.service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

import com.toffy.trainer.extjs.core.service.GoogleSheetAPIService;
import com.toffy.trainer.extjs.emp.model.PositionModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EMPService {
    
    @Autowired
	GoogleSheetAPIService googleSheetAPIService;

    public String getEmployeeList() throws IOException, GeneralSecurityException {
        List<List<Object>> employeeList = googleSheetAPIService.getGoogleSheetDataBySpreadsheetIdAndRange("1yE4MO0bqQLON4xuQUjlz4wZC4B6ejNhbwNGgIFRwQ-U", "employee!A2:K");
        return employeeList.toString();
    } 

    public List<PositionModel> getPositionList() throws IOException, GeneralSecurityException {
        List<List<Object>> employeeList = googleSheetAPIService.getGoogleSheetDataBySpreadsheetIdAndRange("1yE4MO0bqQLON4xuQUjlz4wZC4B6ejNhbwNGgIFRwQ-U", "position!A2:G");
        
        List<PositionModel> responesPosition = new ArrayList<PositionModel>();

        for( List<Object> employee : employeeList) {
            PositionModel employeeModel = new PositionModel();
            // id
            if (employee.get(0) != null) {
                employeeModel.setId(Integer.valueOf(employee.get(0).toString()));

                if(employee.get(1) != null) {
                    employeeModel.setCode(employee.get(1).toString());
                }

                if(employee.get(2) != null) {
                    employeeModel.setDescription(employee.get(2).toString());
                }
            }
            responesPosition.add(employeeModel);
        }
        
        return responesPosition;
    }

}
